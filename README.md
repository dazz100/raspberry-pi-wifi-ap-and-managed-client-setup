# Raspberry Pi Wifi AP and Managed Client Setup

This is a bash script that configures a Raspberry Pi to run both as a normal managed wifi client and as an access point at the same time.